import sys
# This is needed to ensure that dynamic_cast and RTTI works inside kdelibs.

try:
    from DLFCN import RTLD_NOW, RTLD_GLOBAL
except ImportError:
    RTLD_GLOBAL = -1
    RTLD_NOW = -1
    import os
    osname = os.uname()[0]
    if osname == 'Linux' or osname == 'SunOS' or osname == 'FreeBSD' or osname == 'NetBSD':
        RTLD_GLOBAL = 0x100
        RTLD_NOW = 0x2
    elif osname == 'Darwin':
        RTLD_GLOBAL = 0x8
        RTLD_NOW = 0x2
    del os
except:
    RTLD_GLOBAL = -1
    RTLD_NOW = -1

if RTLD_GLOBAL != -1 and RTLD_NOW != -1:
    sys.setdlopenflags(RTLD_NOW|RTLD_GLOBAL)
